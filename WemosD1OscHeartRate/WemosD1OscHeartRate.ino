#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>

//TODO GENERAR PROYECTO MIRANDO PULSE BITRATE ALTERNATIVE

//OSC
WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192,168,1,126);        // remote IP of your computer
const unsigned int outPort = 5006;          // remote port to receive OSC
const unsigned int localPort = 8888;        // local port to listen for OSC packets (actually not used for sending)

//--

//WIFI
char ssid[] = "4DC";          // your network SSID (name)
char pass[] = "esparaelvino";                    // your network password
//--


//  Variables
int PulseSensorPurplePin = A0;        // Pulse Sensor PURPLE WIRE connected to ANALOG PIN 0
int LED13 = D4;   //  The on-board Arduion LED

int Signal;                // holds the incoming raw data. Signal value can range from 0-1024
int Threshold = 800;            // Determine which Signal to "count as a beat", and which to ingore.

int Heartbyte = 0 ;

// The SetUp Function:
void setup() {
  Serial.begin(115200);
  //WIFI
    // Connect to WiFi network
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");

    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    Serial.println("Starting UDP");
    Udp.begin(localPort);
    Serial.print("Local port: ");
    Serial.println(Udp.localPort());

  //--

  // PULSE SENSOR
  analogWrite(PulseSensorPurplePin, 700) ; //CAMBIE LA FRECUENCIA A 700 mhz !
  pinMode(PulseSensorPurplePin, INPUT);
  pinMode(LED13,OUTPUT);         // pin that will blink to your heartbeat!
  // --

}

// The Main Loop Function
void loop() {

  //PULSE SENSOR
  Signal = analogRead(PulseSensorPurplePin);  // Read the PulseSensor's value.
                                              // Assign this value to the "Signal" variable.

  Serial.println(Signal);                    // Send the Signal value to Serial Plotter.
  
  
   
   if(Signal > Threshold){                          // If the signal is above "550", then "turn-on" Arduino's on-Board LED.
    delay(10);
    Signal = analogRead(PulseSensorPurplePin);
    if(Signal > Threshold){
      Heartbyte = 1 ;
     Serial.println("HIGH");
     digitalWrite(LED13,HIGH);
    }
   } else {
    Heartbyte = 0 ;
     digitalWrite(LED13,LOW);                //  Else, the sigal must be below "550", so "turn-off" this LED.
   }
  //--

  //OSC--

    OSCMessage msg("/1");
    msg.add(Heartbyte);
    Udp.beginPacket(outIp, outPort);
    msg.send(Udp);
    Udp.endPacket();
    msg.empty();
  //--


  delay(100);
}



